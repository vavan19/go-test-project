package main

import (
	"fmt"
	"go_test/google_crowler"
	"io/ioutil"
	"net/http"
)

type GoogleDataSource struct {
	queryFormat string
}

func (g GoogleDataSource) GetData(s string) (string, error) {
	resp, err := http.Get(fmt.Sprintf(g.queryFormat, s))
	if err != nil {
		return ``, err
	}
	defer resp.Body.Close()

	bites, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ``, err
	}

	return string(bites), nil
}

func main() {
	source := GoogleDataSource{
		queryFormat: `http://www.google.com/search?q=%v`,
	}
	fmt.Println(google_crowler.Run(source))
}